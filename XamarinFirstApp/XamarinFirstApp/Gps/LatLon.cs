﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace XamarinGps.Gps
{
    public class LatLon : ILatLon
    {
        public double Latitude { get; set; }
        public double Longitude { get; set; }

        public LatLon(double lat, double lon) {
            Latitude = lat;
            Longitude = lon;
        }
        public override bool Equals(Object obj) {
            //Check for null and compare run-time types.
            if ((obj == null) || !(obj is LatLon lon)) {
                return false;
            }
            else {
                LatLon l = lon;
                return (Latitude == l.Latitude) && (Longitude == l.Longitude);
            }
        }
    }
}
