﻿using System;
using System.Collections.Generic;
using System.Net.Http.Headers;
using System.Text;
using XamarinGps.Gps;

namespace XamarinGps
{
    public class GpsData : ILatLon
    {
        public double CourseInDeg { get; set; }
        public double SpeedInMeters { get; set; }
        public double VerticalAccuracy { get; set; }
        public double AccuracyInMeter { get; set; }
        public double AltitudeInMeter { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
        public DateTimeOffset Timestamp { get; set; }
        public bool IsFromMockProvider { get; set; }

        public LatLon ToLatLon() {
            return new LatLon(Latitude, Longitude);
        }
    }
}
