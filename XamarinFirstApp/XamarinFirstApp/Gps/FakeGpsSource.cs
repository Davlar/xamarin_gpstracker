﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using Xamarin.Essentials;

namespace XamarinGps
{
    public class FakeGpsSource : IGpsSource
    {
        public GeolocationAccuracy Accuracy { get; private set; } = GeolocationAccuracy.Best;
        public string Gpxfile { get; set; } = null;

        public Task<GpsData> GetCurrentLocation(TimeSpan timeout, CancellationToken cts) {
            var task = new Task<GpsData>(() => GetGpxNode());
            task.RunSynchronously();
            return task;
        }

        XmlNodeList nodes;
        int index = 0;
        string lastFile = "";
        private GpsData GetGpxNode() {
            if (lastFile != Gpxfile)
                SetupPositionNodes();

            (double lat, double lon) = ExtractLatLon(nodes.Item(index));
            index++;

            return new GpsData() {
                Latitude = lat,
                Longitude = lon
            };
        }

        private void SetupPositionNodes() {
            var gpxDoc = new XmlDocument();
            if (String.IsNullOrEmpty(Gpxfile)) {
                Stream stream = new MemoryStream(Properties.Resources.LinkToNorrkop);
                gpxDoc.Load(stream);
            }
            else
                gpxDoc.Load(Gpxfile);

            XmlNamespaceManager nsmgr = new XmlNamespaceManager(gpxDoc.NameTable);
            nsmgr.AddNamespace("x", "http://www.topografix.com/GPX/1/1");
            nodes = gpxDoc.SelectNodes("//x:trkpt", nsmgr);

            index = 0;
            lastFile = Gpxfile;
        }

        private (double lat, double lon) ExtractLatLon(XmlNode node) {
            var lat = double.Parse((node as XmlElement).GetAttribute("lat"));
            var lon = double.Parse((node as XmlElement).GetAttribute("lon"));
            return (lat, lon);
        }

        public void SetAccuracy(GeolocationAccuracy accuracy) => Accuracy = accuracy;
    }
}
