﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Shapes;

namespace XamarinGps
{
    class GpsSource : IGpsSource
    {
        public GeolocationAccuracy Accuracy { get; private set; } = GeolocationAccuracy.Best;

        private LocationToGpsDataTranslator locToGps = new LocationToGpsDataTranslator();

        public async Task<GpsData> GetCurrentLocation(TimeSpan timeout, CancellationToken cts) {
            Location location = null;
            try {
                var request = new GeolocationRequest(Accuracy, timeout);
                location = await Geolocation.GetLocationAsync(request, cts);
            }
            catch (FeatureNotSupportedException fnsEx) {
                ThrowGpsError(fnsEx);
            }
            catch (FeatureNotEnabledException fneEx) {
                ThrowGpsError(fneEx);
            }
            catch (PermissionException pEx) {
                ThrowGpsError(pEx);
            }
            catch (Exception ex) {
                ThrowGpsError(ex);
            }

            return locToGps.Transform(location);
        }

        private void ThrowGpsError(Exception ex) {
            throw new GpsException("GPS faulted, see inner exception for more information", ex);
        }

        public void SetAccuracy(GeolocationAccuracy accuracy) => Accuracy = accuracy;

        internal class LocationToGpsDataTranslator
        {
            public GpsData Transform(Location location) {
                GpsData gps = new GpsData {
                    CourseInDeg = RemoveNull(location.Course),
                    AccuracyInMeter = RemoveNull(location.Accuracy),
                    VerticalAccuracy = RemoveNull(location.Accuracy),
                    AltitudeInMeter = RemoveNull(location.Altitude),
                    SpeedInMeters = RemoveNull(location.Speed),
                    Longitude = RemoveNull(location.Longitude),
                    Latitude = RemoveNull(location.Latitude),
                    IsFromMockProvider = location.IsFromMockProvider
                };
                return gps;
            }

            private double RemoveNull(double? value) {
                return (double)(value == null ? 0 : value);
            }
        }


    }
}
