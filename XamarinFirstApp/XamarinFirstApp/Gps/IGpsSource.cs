﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Xamarin.Essentials;

namespace XamarinGps
{
    public interface IGpsSource
    {
        GeolocationAccuracy Accuracy { get; }
        Task<GpsData> GetCurrentLocation(TimeSpan timeout, CancellationToken cts);
        void SetAccuracy(GeolocationAccuracy accuracy);
    }
}