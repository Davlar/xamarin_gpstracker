﻿using System;
using XamarinGps.Gps;

namespace XamarinGps
{
    public interface ILatLon
    {
        double Latitude { get; set; }
        double Longitude { get; set; }
    }
}