﻿using System;
using System.ComponentModel;
using Xamarin.Forms;
using System.Threading;
using System.Timers;
using Xamarin.Forms.Maps;
using System.Threading.Tasks;
using XamarinGps.Map;
using System.Collections.Generic;
using XamarinGps.Gps;

namespace XamarinGps
{
    public partial class MainPage : ContentPage, INotifyPropertyChanged {
        private static System.Timers.Timer Timer;
        //private IGpsSource gps = new GpsSource();
        private readonly IGpsSource gps = new FakeGpsSource();
        
        private Guid Guid { get; set; } = Guid.NewGuid();

        private MapController MapControl;

        CancellationTokenSource cts;

        // TODO - Testa att köra msbuild och dotnet test i en docker modul för att se om projektet bygger
        public MainPage() {
            InitializeComponent();
            Timer = new System.Timers.Timer {
                Interval = 1000
            };

            Timer.Elapsed += OnTimerElapsed;
            Timer.Enabled = true;

            // TODO - Skriva tester för resten av det öppna API:et på MapController
            MapControl = new MapController(MainMap) { AutoFollowPosition = true };
            MapControl.StartNewTrack(Guid);
        }

        private void OnTimerElapsed(object sender, ElapsedEventArgs e) {
            Device.BeginInvokeOnMainThread(async () => {
                cts = new CancellationTokenSource();
                try {
                    var location = await gps.GetCurrentLocation(TimeSpan.FromSeconds(10), cts.Token);
                    UpdateLocationText(location);
                    UpdateTimeText(location.Timestamp);

                    MapControl.ReceiveNewPosition(location);
                    MapControl.DrawLastestLineInActiveTrack();
                }
                catch (GpsException ex) {
                    Error.Text = ex.Message;
                    OnPropertyChanged(nameof(Error));
                }
                catch (MapException ex) {
                    Error.Text = ex.Message;
                    OnPropertyChanged(nameof(Error));
                }
            });
            Timer.Start();
        }

        private void UpdateTimeText(DateTimeOffset time) {
            Time.Text = time.LocalDateTime.ToString();
            OnPropertyChanged(nameof(Time));
        }

        private void UpdateLocationText(GpsData location) {
            if (location != null) {
                Location.Text = $"Latitude: {location.Latitude}\n" +
                $"Longitude: {location.Longitude}\n" +
                $"Altitude: {location.AltitudeInMeter}";

                OnPropertyChanged(nameof(Location));
            }
        }

        protected override void OnDisappearing() {
            if (cts != null && !cts.IsCancellationRequested)
                cts.Cancel();
            base.OnDisappearing();
        }
    }
}
