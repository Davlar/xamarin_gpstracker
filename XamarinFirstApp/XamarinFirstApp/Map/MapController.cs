﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Runtime;
using System.Text;
using Xamarin.Forms.Maps;
using XamarinGps.Gps;

namespace XamarinGps.Map
{
    public class MapController
    {
        private Xamarin.Forms.Maps.Map MainMap { get; set; }
        private readonly MapInformation map = new();

        public LatLon Center { get; private set; }
        public bool AutoFollowPosition { get; set; }

        public MapController(Xamarin.Forms.Maps.Map MapToUse) {
            MainMap = MapToUse ?? throw new ArgumentNullException("The map to use can't be null");
        }

        public void ReceiveNewPosition(GpsData location) {
            map.ReceiveNewPosition(location);
            if (AutoFollowPosition)
                UpdateCenterPosition(location.ToLatLon());
        }

        public void UpdateCenterPosition(ILatLon latLon) {
            var lat = MainMap.VisibleRegion?.LatitudeDegrees ?? 0.3;
            var lon = MainMap.VisibleRegion?.LongitudeDegrees ?? 0.3;
            MainMap.MoveToRegion(new MapSpan(new Position(latLon.Latitude, latLon.Longitude), lat, lon));
            Center = new(latLon.Latitude, latLon.Longitude);
        }

        public void DrawLastestLineInActiveTrack() {
            if (map.ActiveTrack == Guid.Empty || map.ActiveTrack == null)
                throw new InvalidOperationException("There is no active track!");

            var (secondLast, last) = map.GetLastPositionPair(map.ActiveTrack);
            CreatePolylineOnMap(secondLast, last);
        }

        public void CreatePolylineOnMap(ILatLon secondLast, ILatLon last) {
            var polyline = new Polyline {
                StrokeColor = Color.Red,
                StrokeWidth = 12
            };
            polyline.Geopath.Add(new Position(secondLast.Latitude, secondLast.Longitude));
            polyline.Geopath.Add(new Position(last.Latitude, last.Longitude));
            MainMap.MapElements.Add(polyline);
        }

        public void StartNewTrack(Guid guid) => map.StartNewTrack(guid);

        public (ILatLon secondLast, ILatLon last) GetLastPositionPair(Guid guid) => map.GetLastPositionPair(guid);
    }
}
