﻿using System;

namespace XamarinGps
{
    public class MapException : Exception
    {
        public MapException() {
        }

        public MapException(string message) : base(message) {
        }

        public MapException(string message, Exception inner) : base(message, inner) {
        }
    }
}
