﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using XamarinGps.Gps;

namespace XamarinGps.Map
{
    public class MapInformation
    {
        public GpsData LastKnownPosition { get; private set; }
        public Guid ActiveTrack { get; private set; }

        private Dictionary<Guid, List<GpsData>> tracks = new();

        public MapInformation() {

        }

        public void StartNewTrack(Guid guid) {
            if (tracks.Keys.Contains(guid))
                throw new InvalidOperationException("This guid is already being tracked");

            ActiveTrack = guid;
            tracks.Add(ActiveTrack, new List<GpsData>());
        }

        public void ReceiveNewPosition(GpsData data) {
            LastKnownPosition = data;

            if (ActiveTrack != Guid.Empty)
                tracks[ActiveTrack].Add(data);
        }

        public List<GpsData> GetPositionsInTrack(Guid guid) {
            return tracks[guid].ToList();
        }

        public (ILatLon secondLast, ILatLon last) GetLastPositionPair(Guid guid) { 
            var list = GetPositionsInTrack(guid);
            if (list.Count < 2)
                throw new MapException("It doesn't exist two position in this track yet");
            var n = list.Count;
            return (list[n - 2].ToLatLon(), list[n - 1].ToLatLon());
        }
    }
}
