﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using XamarinGps;
using System;
using System.Collections.Generic;
using System.Text;

namespace UnitTest.Gps
{
    [TestClass()]
    public abstract class IGpsSourceTests
    {
        public IGpsSource GpsSource { get; private set; }
        public abstract IGpsSource GetSourceInstance();

        [TestInitialize]
        public void Setup() {
            GpsSource = GetSourceInstance();
        }
    }
}