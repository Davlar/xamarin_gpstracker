﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Threading;
using XamarinGps;
using System.Security;
using System.Reflection;

namespace UnitTest.Gps
{
    [TestClass]
    public class FakeGpsSourceTest : IGpsSourceTests
    {
        public override IGpsSource GetSourceInstance() {
            return new FakeGpsSource();
        }

        [TestMethod]
        public void ReadGpxFile_DefaultLinkToNorrkoping_LatLonExists() {
            GpsData gps = null;
            Task.Run(async () => { gps = await GpsSource.GetCurrentLocation(new System.TimeSpan(), new System.Threading.CancellationToken()); }).GetAwaiter().GetResult();
            Assert.IsTrue(gps.Longitude != 0);
            Assert.IsTrue(gps.Longitude != 0);
        }

        [TestMethod]
        public void ReadGpxFile_DefaultinkToNorrkoping_LatLonChanges() {
            List<GpsData> gps = new List<GpsData>();
            for (var n = 0; n < 719; ++n)
                Task.Run(async () => { gps.Add(await GpsSource.GetCurrentLocation(new TimeSpan(), new CancellationToken())); }).GetAwaiter().GetResult();

            Assert.AreEqual((58.39035, 15.56591), (gps[0].Latitude, gps[0].Longitude));
            Assert.AreEqual((58.58109, 16.23974), (gps[^1].Latitude, gps[^1].Longitude));
        }
    }
}
