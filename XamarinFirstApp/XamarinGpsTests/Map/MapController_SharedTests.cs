﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using XamarinGps.Map;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XamarinGps.Gps;
using XamarinGps;
using Xamarin.Forms.Maps;
using System.Net.NetworkInformation;
using Xamarin.Forms;
using System.Runtime.CompilerServices;
using System.Reflection.Metadata;
using System.Linq.Expressions;
using System.Runtime.InteropServices;

namespace UnitTest.Map
{
    [TestClass()]
    public abstract class MapController_SharedTests
    {
        public ILatLon Position { get; private set; }
        public abstract ILatLon GetPositionInstance();
        public MapController MapCon { get; private set; }

        [TestInitialize]
        public void Setup() {
            Position = GetPositionInstance();
            MapCon = new(new Xamarin.Forms.Maps.Map());
        }

        [TestMethod()]
        public void UpdateCenterPosition_ValidInput() {
            MapCon.UpdateCenterPosition(Position);
            var pos = MapCon.Center;
            Assert.AreEqual(Position.Latitude, pos.Latitude);
            Assert.AreEqual(Position.Longitude, pos.Longitude);
        }

        [TestMethod()]
        public void UpdateCenterPosition_NullInput() {
            Position = null;
            Assert.ThrowsException<NullReferenceException>(() => { MapCon.UpdateCenterPosition(Position); });
        }
    }
}