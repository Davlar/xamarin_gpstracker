﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using XamarinGps.Map;
using System;
using System.Collections.Generic;
using System.Text;
using XamarinGps;
using System.Linq;
using XamarinGps.Gps;
using System.ComponentModel.DataAnnotations;

namespace UnitTest.Map
{
    [TestClass()]
    public class MapInformationTests
    {

        MapInformation Map = new MapInformation();
        Guid guid = Guid.NewGuid();

        List<GpsData> positions => GetPositions();

        GpsData gpsData = new GpsData() {
            Latitude = 42,
            Longitude = 42
        };

        [TestInitialize]
        public void Setup() {
            Map = new();
            Map.StartNewTrack(guid);
        }

        [TestMethod()]
        public void ReceiveNewPosition_LastKnownPositionUpdated() {
            Map.ReceiveNewPosition(gpsData);
            Assert.AreEqual(gpsData, Map.LastKnownPosition);
        }

        [TestMethod()]
        public void StartNewTrack_TwoTimes_ThrowsException() {
            Assert.ThrowsException<InvalidOperationException>(() => Map.StartNewTrack(guid));
        }

        [TestMethod()]
        public void ReceiveNewPosition_AddPointsToTrack() {
            AddPositionsToMap();
            Assert.AreEqual(positions.Last().Latitude, Map.LastKnownPosition.Latitude);
            Assert.AreEqual(positions.Last().Longitude, Map.LastKnownPosition.Longitude);
        }

        [TestMethod()]
        public void GetPositionsInTrack_CountIsCorrect() {
            AddPositionsToMap();
            var list = Map.GetPositionsInTrack(guid);
            Assert.AreEqual(positions.Count, list.Count);
        }

        [TestMethod()]
        public void GetPositionsInTrack_CreateSecondTrack_CountIsZero() {
            AddPositionsToMap();
            var g = Guid.NewGuid();
            Map.StartNewTrack(g);
            var list = Map.GetPositionsInTrack(g);
            Assert.AreEqual(0, list.Count);
        }

        [TestMethod()]
        public void GetLastPositionPair_ReturnTupleOfPositions() {
            AddPositionsToMap();
            (ILatLon secondLast, ILatLon last) = Map.GetLastPositionPair(guid);
            var n = positions.Count;
            Assert.AreEqual(secondLast.Latitude, positions[n-2].ToLatLon().Latitude);
            Assert.AreEqual(secondLast.Longitude, positions[n-2].ToLatLon().Longitude);
            Assert.AreEqual(last.Latitude, positions[n - 1].ToLatLon().Latitude);
            Assert.AreEqual(last.Longitude, positions[n - 1].ToLatLon().Longitude);
        }

        [TestMethod()]
        public void GetLastPositionPair_OnlyOnePosition_ThrowError() {
            var guid = Guid.NewGuid();
            Map.StartNewTrack(guid);
            Map.ReceiveNewPosition(gpsData);
            Assert.ThrowsException<MapException>(() => Map.GetLastPositionPair(guid));
        }

        private void AddPositionsToMap() {
            foreach (var gps in positions)
                Map.ReceiveNewPosition(gps);
        }

        private List<GpsData> GetPositions() {
            List<GpsData> gps = new();
            gps.Add(new GpsData() {
                Latitude = 58.39035,
                Longitude = 15.56591
            });
            gps.Add(new GpsData() {
                Latitude = 58.39032,
                Longitude = 15.56656
            });
            gps.Add(new GpsData() {
                Latitude = 58.39012,
                Longitude = 15.57014
            });
            return gps;
        }
    }
}