﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnitTest.Map;
using XamarinGps;

namespace UnitTest.Map
{
    [TestClass]
    public class MapController_GpsDataTests : MapController_SharedTests
    {
        GpsData gps => (GpsData)Position;

        public override ILatLon GetPositionInstance() {
            return new GpsData() { Longitude = 58.410807, Latitude = 15.621373 };
        }

        [TestMethod]
        public void ReceiveNewPosition_AutoFollowOff() {
            MapCon.AutoFollowPosition = false;
            MapCon.ReceiveNewPosition(gps);
            Assert.IsNull(MapCon.Center);
        }


        [TestMethod]
        public void ReceiveNewPosition_AutoFollowOn() {
            MapCon.AutoFollowPosition = true;
            MapCon.ReceiveNewPosition(gps);
            Assert.AreEqual(MapCon.Center, gps.ToLatLon());
        }
    }
}
