﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnitTest.Map;
using XamarinGps;
using XamarinGps.Gps;

namespace UnitTest.Map
{

    [TestClass]
    public class MapController_LatLonTests : MapController_SharedTests
    {
        public override ILatLon GetPositionInstance() {
            return new LatLon(58.410807, 15.621373);
        }
    }
}
